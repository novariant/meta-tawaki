SECTION = "macroframework"
DESCRIPTION = "Library for tracing system events (config-driven wrapper of lttng)"

LICENSE = "GPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"
PR = "r0"

DEPENDS = "lttng-ust"

SRC_URI = "git://git@git.novarianteng.net/novariant/trace.git;protocol=ssh"
SRCREV = "HEAD"

S = "${WORKDIR}/git"

inherit autotools
