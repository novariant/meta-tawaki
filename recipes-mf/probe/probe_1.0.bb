SECTION = "macroframework"
DESCRIPTION = "Library for probing core data structures."

LICENSE = "Novariant"
LIC_FILES_CHKSUM = "file://COPYING;md5=d41d8cd98f00b204e9800998ecf8427e"
PR = "r0"

SRC_URI = "git://git@git.novarianteng.net/novariant/probe.git;protocol=ssh"
SRCREV = "HEAD"

S = "${WORKDIR}/git"

inherit autotools
